var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/components'));
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/healthcheck', function (req, res) {
  res.send('OK');
});

app.listen( port, function () {
  console.log('server listening on port :',port);
});