# The FROM is the base image our image will be based upon
FROM ubuntu:14.04

# Next RUN will execute commands during the docker build step
# In this RUN will will be prepping the Ubuntu image to accept
# the changes we will be making
RUN \
  sed -i 's/# \(.*multiverse\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential && \
  apt-get install -y wget && \
  rm -rf /var/lib/apt/lists/*

# This RUN will now install Node (and NPM) and place them on the system
# We then clean up the unneeded artifact
RUN \
  cd /opt && \
  wget http://nodejs.org/dist/v4.2.5/node-v4.2.5-linux-x64.tar.gz && \
  tar -xzf node-v4.2.5-linux-x64.tar.gz && \
  mv node-v4.2.5-linux-x64 node && \
  cd /usr/local/bin && \
  ln -s /opt/node/bin/* . && \
  rm -f /opt/node-v4.2.5-linux-x64.tar.gz

# We will now create the area our app will be installed
RUN \
  cd /opt && \
  mkdir components-library && \
  cd components-library

# ADD our local files to the Docker image in the directory we just created
ADD . /opt/components-library/

# Switch our context to the correct directory
WORKDIR /opt/components-library

# Now RUN our final build command to correctly pull in the Node modules our app needs
RUN \
  npm install

# This final command will be executed when our Image is run as a Container
CMD ["node", "app.js"]